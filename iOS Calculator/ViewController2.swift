//
//  ViewController2.swift
//  iOS Calculator
//
//  Created by Emerson on 6/7/20.
//  Copyright © 2020 Emerson. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

       //Result
        @IBOutlet weak var lblResultado: UILabel!
        
        //Numbers
        @IBOutlet weak var btn0: UIButton!
        @IBOutlet weak var btn1: UIButton!
        @IBOutlet weak var btn2: UIButton!
        @IBOutlet weak var btn3: UIButton!
        @IBOutlet weak var btn4: UIButton!
        @IBOutlet weak var btn5: UIButton!
        @IBOutlet weak var btn6: UIButton!
        @IBOutlet weak var btn7: UIButton!
        @IBOutlet weak var btn8: UIButton!
        @IBOutlet weak var btn9: UIButton!
    
        
        //Operators
        @IBOutlet weak var btnDecimal: UIButton!
        @IBOutlet weak var btnAC: UIButton!
        @IBOutlet weak var btnMasmenos: UIButton!
        @IBOutlet weak var btnPorcentaje: UIButton!
        @IBOutlet weak var btnDivision: UIButton!
        @IBOutlet weak var btnMultiplicacion: UIButton!
        @IBOutlet weak var btnResta: UIButton!
        @IBOutlet weak var btnSuma: UIButton!
        @IBOutlet weak var btnIgual: UIButton!

        //Variables
        
        private var total:Double = 0
        private var temp:Double = 0
        private var operating = false
        private var decimal = false
        private var operation:OperationType = .none
        
        //Constant
        private let kDecimalSeparator = Locale.current.decimalSeparator!
        private let kMaxLength = 9
        private let kTotal = "total"
        
        private enum OperationType {
            case none,addiction,substraction,multiplication,division,percent
        }
        
        // Formateo de valores auxiliares
        private let auxFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            let locale = Locale.current
            formatter.groupingSeparator = ""
            formatter.decimalSeparator = locale.decimalSeparator
            formatter.numberStyle = .decimal
            formatter.maximumIntegerDigits = 100
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 100
            return formatter
        }()
        
        // Formateo de valores auxiliares totales
        private let auxTotalFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.groupingSeparator = ""
            formatter.decimalSeparator = ""
            formatter.numberStyle = .decimal
            formatter.maximumIntegerDigits = 100
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 100
            return formatter
        }()
        
        // Formateo de valores por pantalla por defecto
        private let printFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            let locale = Locale.current
            formatter.groupingSeparator = locale.groupingSeparator
            formatter.decimalSeparator = locale.decimalSeparator
            formatter.numberStyle = .decimal
            formatter.maximumIntegerDigits = 9
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 8
            return formatter
        }()
        
        // Formateo de valores por pantalla en formato científico
        private let printScientificFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .scientific
            formatter.maximumFractionDigits = 3
            formatter.exponentSymbol = "e"
            return formatter
        }()
        
        override func viewDidLoad() {
            
            super.viewDidLoad()
            btnDecimal.setTitle(kDecimalSeparator, for: .normal)
            
            total = UserDefaults.standard.double(forKey: kTotal)
            
            result()
            
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            
            // UI
            btn0.round()
            btn1.round()
            btn2.round()
            btn3.round()
            btn4.round()
            btn5.round()
            btn6.round()
            btn7.round()
            btn8.round()
            btn9.round()
            
            btnDecimal.round()
            btnAC.round()
            btnMasmenos.round()
            btnPorcentaje.round()
            btnDivision.round()
            btnMultiplicacion.round()
            btnResta.round()
            btnSuma.round()
            btnIgual.round()
            
        }
        
        @IBAction func actionAc(_ sender: UIButton) {
            clear()
            sender.shine()
        }
        @IBAction func actionMasmenos(_ sender: UIButton) {
            temp = temp * (-1)
            
            lblResultado.text = printFormatter.string(from: NSNumber(value: temp))
            sender.shine()
        }
        @IBAction func actionPorcentaje(_ sender: UIButton) {
            if operation != .percent {
                result()
            }
            operating = true
            operation = .percent
            result()
            sender.shine()
        }
        @IBAction func actionDivision(_ sender: UIButton) {
            if operation != .none {
                result()
            }
            
            operating = true
            operation = .division
            sender.selectOperation(true)
            sender.shine()
        }
        @IBAction func actionMultiplicacion(_ sender: UIButton) {
            if operation != .none {
                result()
            }
            
            operating = true
            operation = .multiplication
            sender.selectOperation(true)
            sender.shine()
        }
        @IBAction func actionResta(_ sender: UIButton) {
            if operation != .none {
                result()
            }
            
            operating = true
            operation = .substraction
            sender.selectOperation(true)
            sender.shine()
        }
        @IBAction func actionSuma(_ sender: UIButton) {
            if operation != .none {
                result()
            }
            operating = true
            operation = .addiction
            sender.selectOperation(true)
            sender.shine()
        }
        @IBAction func actionIgual(_ sender: UIButton) {
            result()
            sender.shine()
        }
    
        @IBAction func numberAction(_ sender: UIButton) {
            btnAC.setTitle("C", for: .normal)
            
            var currentTemp = auxTotalFormatter.string(from: NSNumber(value: temp))!
            if !operating && currentTemp.count >= kMaxLength {
                return
            }
            
            currentTemp = auxFormatter.string(from: NSNumber(value: temp))!
            
            // Hemos seleccionado una operación
            if operating {
                total = total == 0 ? temp : total
                lblResultado.text = ""
                currentTemp = ""
                operating = false
            }
            
            // Hemos seleccionado decimales
            if decimal {
                currentTemp = "\(currentTemp)\(kDecimalSeparator)"
                decimal = false
            }
            
            let number = sender.tag
            temp = Double(currentTemp + String(number))!
            lblResultado.text = printFormatter.string(from: NSNumber(value: temp))
            
            selectVisualOperation()
            
            sender.shine()
        }

        // Limpia los valores
        private func clear() {
            if operation == .none {
                total = 0
            }
            operation = .none
            btnAC.setTitle("AC", for: .normal)
            if temp != 0 {
                temp = 0
                lblResultado.text = "0"
            } else {
                total = 0
                result()
            }
        }
        
        // Obtiene el resultado final
        private func result() {
            
            switch operation {

            case .none:
                // No hacemos nada
                break
            case .addiction:
                total = total + temp
                break
            case .substraction:
                total = total - temp
                break
            case .multiplication:
                total = total * temp
                break
            case .division:
                total = total / temp
                break
            case .percent:
                temp = temp / 100
                total = temp
                break
            }
            
            // Formateo en pantalla
            if let currentTotal = auxTotalFormatter.string(from: NSNumber(value: total)), currentTotal.count > kMaxLength {
                lblResultado.text = printScientificFormatter.string(from: NSNumber(value: total))
            } else {
                lblResultado.text = printFormatter.string(from: NSNumber(value: total))
            }
            
            operation = .none
            
            selectVisualOperation()
            
            UserDefaults.standard.set(total, forKey: kTotal)
            
            print("TOTAL: \(total)")
        }
        
        // Muestra de forma visual la operación seleccionada
        private func selectVisualOperation() {
            
            if !operating {
                // No estamos operando
                btnSuma.selectOperation(false)
                btnResta.selectOperation(false)
                btnMultiplicacion.selectOperation(false)
                btnDivision.selectOperation(false)
                
            } else {
                switch operation {
                case .none, .percent:
                    btnSuma.selectOperation(false)
                    btnResta.selectOperation(false)
                    btnMultiplicacion.selectOperation(false)
                    btnDivision.selectOperation(false)
                    
                    break
                case .addiction:
                    btnSuma.selectOperation(true)
                    btnResta.selectOperation(false)
                    btnMultiplicacion.selectOperation(false)
                    btnDivision.selectOperation(false)
                    
                    break
                case .substraction:
                    btnSuma.selectOperation(false)
                    btnResta.selectOperation(true)
                    btnMultiplicacion.selectOperation(false)
                    btnDivision.selectOperation(false)
                    
                    break
                case .multiplication:
                    btnSuma.selectOperation(false)
                    btnResta.selectOperation(false)
                    btnMultiplicacion.selectOperation(true)
                    btnDivision.selectOperation(false)
                    
                    break
                case .division:
                    btnSuma.selectOperation(false)
                    btnResta.selectOperation(false)
                    btnMultiplicacion.selectOperation(false)
                    btnDivision.selectOperation(true)
                    
                    break
                }
            }
        }
        
    }
